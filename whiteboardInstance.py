from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication, QTimer
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction

# Initialize Qt resources from file resources.py
from .resources import *
import requests
import threading
import time
import json
# Import the code for the dialog
from .WhiteBoard_dialog import WhiteBoardDialog
from qgis.PyQt.QtCore import QSettings
import os.path
import socket
import uuid
import os
import tempfile
from qgis.core import QgsVectorLayer,QgsProject, QgsApplication, QgsMessageLog

class WhiteboardInstance(object):
    def __init__(self):
        self.instanceID = None
        self.mapIDs = None
        self.compositionsList = list()
        self.compositionsIds = list()
        self.current = 0

    def getCurrentMap(self):
        return self.compositionsIds[self.current]
    def setInstanceId(self, id):
        self.instanceID = id

    def instanceId(self):
        return self.instanceID

    def setCurrent(self, id):
        i = 0
        for comp in self.compositionsList:
            if comp.id() == id:
                self.current = i
            i += 1

    def getCompositionInfo(self, id):
        for comp in self.compositionsList:
            if comp.id() == id:
                return comp.name, comp.desc

    def getCompositionId(self, name):     
        for comp in self.compositionsList:  
            if comp.name == name:
                return comp.id()
    def setComposition(self, id, name):
        comp = Composition()
        comp.setId(id)
        comp.setName(name)
        #♣comp.setDesc(desc)
        self.compositionsList.append(comp)
        self.compositionsIds.append(id)

    def setLayer(self, compId, layerId, layer):
        for comp in self.compositionsList:
            if comp.id() == compId:
                comp.setLayer(layerId, layer)

    def getLayer(self, compId, layerId):
        layers = list()
        for comp in self.compositionsList:
            if comp.id() == compId:
                layers.append(comp.getLayer(layerId))
                
                #layer = comp.getLayer(layerId) 
        return layers
    def getLayerByQid(self, qId):
        return self.compositionsList[self.current].getLayerIdbyQID(qId)
    def getMapId(self):
        return self.compositionsList[self.current].id()

        
class Composition():
    def __init__(self):
        self.mapID = None
        self.layers = list()
        self.layersIds = list()
        self.name = None
        self.desc = None
    def setName(self, name):
        self.name = name

    def setDesc(self, desc):
        self.desc = desc

    def setId(self, id):
        self.mapID = id

    def setLayer(self, id, layer):
        self.layers.append(layer)
        self.layersIds.append(id)  

    
    def getLayer(self, id):
        pom = 0
        for l in self.layersIds:
            if l == id:
                return self.layers[pom] 
            pom += 1

    def getLayerIdbyQID(self, qId):  
        pom = 0
        for l in self.layers:
            if l.id() == qId:
                return  self.layersIds[pom]
            pom += 1
           
    def id(self):
        return self.mapID
    def name(self):
        return self.name
    def desc(self):
        return self.desc

class Layer():
    def __init__(self):
        self.layer = None





class WFeature():
    def __init__(self):
        ######## array store QgsFeature, QgsFeature ID, Whiteboard feat ID, Whiteboard layer ID, Whiteboard map ID
        self.feat = None
        self.Id = None
        self.layerid = None
        self.mapId = None
        self.feats = list()
    def setFeature(self, feat, qid, wid, layerid, mapId):        
        self.feats.append([feat, qid, wid, layerid, mapId])
    def getWid(self, qid):
        for f in self.feats:
            #print(f)
            #print(qid)
            if f[1] == qid:
                return f[2]

    def removeLayerFeatures(self, layerId):
        for i in range(0, len(self.feats)):
            if self.feats[i][3] == layerId:
                del self.feats[i]
    def removeFeatByQid(self,qid):
        for i in range(0, len(self.feats)):
            if self.feats[i][1] == qid:
                del self.feats[i]

    def getQid(self, wid):
        for f in self.feats:            
            if f[2] == wid:
                return f[1]
    def getFeatureByWid(self, wid):
            for f in self.feats:   
                print(f[1])
                if f[2] == wid:
                    return f[0]        
    def getIds(self):
        ids = list()
        for f in self.feats:
            ids.append(f[2])
        return ids
    def getWlayerId(self, qid):
        #print(qid)
        for f in self.feats:
            #print(f)
            if f[1] == qid:
                return f[3]

    def getWMapId(self, qid):
        for f in self.feats:
            if f[1] == qid:
                return f[4]





     